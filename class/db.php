<?php

#############################
#	aPHPeal Template 		#
#	Copyright Razoft 2014 	#
#############################

#---------------------------#

# MySQL Class #

#---------------------------#

class mysqli_db {
	private $c;

	function __construct(){
		$this->c = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	}

	function clear($str){
		$str = strip_tags($str);
		$str = mysqli_real_escape_string($this->c, $str);

		return $str;
	}

	function fetch($query){
		$raws = mysqli_query($this->c, $this->clear($query));

		if(!$raws){
			return false;
		} else {
			$res = array();
			foreach($raws as $raw){
				array_push($res, $raw);
			}

		return $res;
		}
		
	}
}

class no_db {
	function clear($str){
		$str = strip_tags($str);

		return $str;
	}
}