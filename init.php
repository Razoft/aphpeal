<?php
#############################
#	aPHPeal Template 		#
#	Copyright Razoft 2014	#
#############################

// Include the settings file
include("settings.php");

// Include the Language Pack
include("lang/".LANG.".php");

// Enable Developer Mode
if(DEV){
	error_reporting(E_ALL);
	ini_set("display_errors", "1");
}

// Enable Database Connection
if(USE_DB){
	include("class/db.php");

	$_msg[] = $_l['db_init'];

	// Switch to correct Database Type
	switch(DB_TYPE){
		case 'mysqli':
			$db = new mysqli_db;
			break;
	}
} else {
	$db = new no_db;
}

// Include the main functions file
include("func/func.php");

// Set the current page variable
$p = p();

// Include the page loading handle
include("inc/handle.php");