<?php
#############################
#	aPHPeal Template 		#
#	Copyright Razoft 2014	#
#############################

## Main Configuration File ##





#---------------------------#

# General Settings #

#---------------------------#

// Hostname
define("HOST", "localhost");

// Port
define("PORT", 80);

// Development Mode
define("DEV", true);

// IP Address
define("IP", "127.0.0.1");

// Use IP?
define("USE_IP", false);





#---------------------------#

# Database Connection #

#---------------------------#

// Use Database?
define("USE_DB", true);

// Database Types
define("DB_TYPE", "mysqli");

// Database Host
define("DB_HOST", "127.0.0.1");

// Database User
define("DB_USER", "root");

// Database Password
define("DB_PASS", "");

// Database Name
define("DB_NAME", "aPHPeal");

// Database File
define("DB_FILE", "");





#---------------------------#

# Additional Settings

#---------------------------#

// Use Google Fonts?
define("USE_GFONTS", true);

// Google fonts
define("GFONTS", "Roboto:100|Open+Sans:300,400");

// Default Language
define("LANG", "en");