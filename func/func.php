<?php
#############################
#	aPHPeal Template 		#
#	Copyright Razoft 2014	#
#############################

// Shortcut function to $db->clear();
function c($str){
	global $db;

	return $db->clear($str);
}

// Get current page
function p(){
	return @explode("/", $_SERVER['REQUEST_URI']);
}