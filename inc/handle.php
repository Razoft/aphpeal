<?php
#############################
#	aPHPeal Template 		#
#	Copyright Razoft 2014	#
#############################

if($p[1] == ""){
	include("landing.php");
} else if(in_array($p[1].".php", scandir("../inc/pages"))){
	include("pages/".$p[1].".php");
} else {
	include("404.php");
}